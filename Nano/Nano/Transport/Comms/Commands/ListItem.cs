﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Comms
{
    /// <summary>
    /// Represents a item in a command list. e.g. streams, variables, etc.
    /// </summary>
    public struct ListItem
    {
        /// <summary>
        /// Gets or sets the id of the list item.
        /// </summary>
        public ushort ID;

        /// <summary>
        /// Gets or sets the name of the list item.
        /// </summary>
        public string Name;
    }
}
