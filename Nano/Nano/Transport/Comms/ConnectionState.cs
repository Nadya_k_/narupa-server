﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Comms
{
    public enum ConnectionState
    {
        /// <summary>
        /// Not connected.
        /// </summary>
        NotConnected,

        /// <summary>
        /// Making connection handshake.
        /// </summary>
        Handshake,

        /// <summary>
        /// Connecting.
        /// </summary>
        Connecting,

        /// <summary>
        /// Connected.
        /// </summary>
        Connected,

        /// <summary>
        /// Disconnected.
        /// </summary>
        Disconnected,

        /// <summary>
        /// Disconnecting.
        /// </summary>
        Disconnecting,

        /// <summary>
        /// Forced disconnection.
        /// </summary>
        ForcedDisconnection,
    }
}