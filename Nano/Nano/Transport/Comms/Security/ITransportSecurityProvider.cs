﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Comms.Security
{
    public delegate void LoadSimulationEvent(string simulationPath);

    public delegate void ReservationExpiredEvent();

    public delegate void ReservationMadeEvent();

    public delegate void ReservationReleasedEvent();

    public delegate void ReservedSessionEndedEvent();

    public enum TransportSecurityProviderReservationState
    {
        Unreserved,
        Reserved,
        SessionActive,
    }

    public interface ITransportSecurityProvider
    {
        event LoadSimulationEvent LoadSimulation;

        event ReservationExpiredEvent ReservationExpired;

        event ReservationMadeEvent ReservationMade;

        event ReservationReleasedEvent ReservationReleased;

        event ReservedSessionEndedEvent ReservedSessionEnded;

        int ActiveSessions { get; }

        TransportSecurityProviderReservationState State { get; }

        void CheckReservationState();

        void ReleaseSession(TransportSecuritySessionToken token);

        bool TryAquireSession(out TransportSecuritySessionToken token);

        bool TryServerReservation(string simulationPath, out string reservationToken);
    }

    public class TransportSecuritySessionToken
    {
        private ITransportSecurityProvider securityProvider;

        public string SecurityKey { get; }

        internal TransportSecuritySessionToken(ITransportSecurityProvider securityProvider, string securityKey)
        {
            this.securityProvider = securityProvider;

            SecurityKey = securityKey;
        }

        internal void Release()
        {
            securityProvider.ReleaseSession(this);
        }
    }
}