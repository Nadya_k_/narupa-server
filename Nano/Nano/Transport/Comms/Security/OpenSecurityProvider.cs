﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.WebSockets;
using System.Collections.Generic;

namespace Nano.Transport.Comms.Security
{
    public class OpenSecurityProvider : ITransportSecurityProvider
    {
        private readonly List<TransportSecuritySessionToken> activeTokens = new List<TransportSecuritySessionToken>();
        private readonly string openClientKey;
        private readonly object syncLock = new object();

        public event LoadSimulationEvent LoadSimulation;

        public event ReservationExpiredEvent ReservationExpired;

        public event ReservationMadeEvent ReservationMade;

        public event ReservationReleasedEvent ReservationReleased;

        /// <inheritdoc />
        public event ReservedSessionEndedEvent ReservedSessionEnded;

        /// <inheritdoc />
        public int ActiveSessions
        {
            get { lock (syncLock) { return activeTokens.Count; } }
        }

        public TransportSecurityProviderReservationState State => ActiveSessions > 0 ? TransportSecurityProviderReservationState.SessionActive : TransportSecurityProviderReservationState.Unreserved;

        public OpenSecurityProvider()
        {
            openClientKey = Keys.GetOpenClientKey();
        }

        public void CheckReservationState()
        {
        }

        /// <inheritdoc />
        public void ReleaseSession(TransportSecuritySessionToken token)
        {
            lock (syncLock)
            {
                activeTokens.Remove(token);
            }
        }

        /// <inheritdoc />
        public bool TryAquireSession(out TransportSecuritySessionToken token)
        {
            lock (syncLock)
            {
                token = new TransportSecuritySessionToken(this, openClientKey);

                activeTokens.Add(token);

                return true;
            }
        }

        /// <inheritdoc />
        public bool TryServerReservation(string simulationPath, out string reservationToken)
        {
            lock (syncLock)
            {
                reservationToken = openClientKey;

                if (string.IsNullOrEmpty(simulationPath) == false)
                {
                    LoadSimulation?.Invoke(simulationPath);
                }

                ReservationMade?.Invoke(); 

                return true;
            }
        }
    }
}