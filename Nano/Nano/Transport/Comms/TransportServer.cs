﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Nano.Transport.Agents;
using Rug.Osc;
using Nano.Transport.Comms.Security;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Enumeration of simulation server states.
    /// </summary>
    /// <autogeneratedoc />
    public enum SimulationServerState
    {
        /// <summary>
        /// Server is not listening for client connections.
        /// </summary>
        /// <autogeneratedoc />
        NotListening,

        /// <summary>
        /// Server is listening for client connections.
        /// </summary>
        /// <autogeneratedoc />
        Listening,

        /// <summary>
        /// Server is closing.
        /// </summary>
        /// <autogeneratedoc />
        Closing,
    }

    /// <summary>
    /// Simbox server.
    /// </summary>
    /// <autogeneratedoc />
    public class TransportServer : ITransportServer
    {
        /// <summary>
        /// The IP address.
        /// </summary>
        /// <autogeneratedoc />
        public IPAddress Address { get; } 

        /// <summary>
        /// The port.
        /// </summary>
        /// <autogeneratedoc />
        public int Port { get; }

        /// <summary>
        /// The reporter.
        /// </summary>
        /// <autogeneratedoc />
        public IReporter Reporter { get; } 

        private readonly ManualResetEvent doneEvent = new ManualResetEvent(true);
        private readonly ManualResetEvent errorEvent = new ManualResetEvent(true);
        private readonly Dictionary<IPEndPoint, ClientSession> sessionLookup = new Dictionary<IPEndPoint, ClientSession>();
        private readonly object syncLock = new object();
        private IAsyncResult acceptResult;

        private readonly ITransportPacketReceiver receiver;
        private readonly IClientSessionHandler sessionHandler;
        private TcpListener tcpListener;

        private readonly PacketStatistics packetStatistics; 

        /// <summary>
        /// Gets a flag indicating if the transmitter is connected.
        /// </summary>
        /// <value><c>true</c> if this instance is connected; otherwise, <c>false</c>.</value>
        /// <autogeneratedoc />
        public bool IsConnected => State == SimulationServerState.Listening;

        /// <summary>
        /// Gets the server state.
        /// </summary>
        /// <value>The state.</value>
        /// <autogeneratedoc />
        public SimulationServerState State { get; private set; }

        public ConnectionType ConnectionType { get; }

        public ITransportSecurityProvider TransportSecurityProvider { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportServer"/> class.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="port">The port.</param>
        /// <param name="receiver"></param>
        /// <param name="sessionHandler"></param>        
        /// <param name="packetStatistics"></param>
        /// <param name="transportSecurityProvider"></param>
        /// <param name="reporter">The reporter.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">port</exception>
        /// <exception cref="System.ArgumentNullException">reporter;A reporter instance is required.</exception>
        /// <autogeneratedoc />
        public TransportServer(ConnectionType connectionType, IPAddress address,
            int port,
            PacketStatistics packetStatistics,
            ITransportPacketReceiver receiver,
            IClientSessionHandler sessionHandler,
            ITransportSecurityProvider transportSecurityProvider,            
            IReporter reporter)
        {
            if (port < 0 || port > ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(port));
            }

            TransportSecurityProvider = transportSecurityProvider; 

            Address = address;
            Port = port;
            ConnectionType = connectionType;

            Reporter = reporter ?? throw new ArgumentNullException(nameof(reporter), "A reporter instance is required.");
            this.packetStatistics = packetStatistics ?? throw new ArgumentNullException(nameof(packetStatistics), "A PacketStatistics instance is required.");
            this.receiver = receiver ?? throw new ArgumentNullException(nameof(receiver), "A receiver instance is required.");
            this.sessionHandler = sessionHandler ?? throw new ArgumentNullException(nameof(sessionHandler), "A sessionHandler instance is required."); 

            State = SimulationServerState.NotListening;
        }

        /// <summary>
        /// Broadcast a block of data to all receivers. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="packets">The packets.</param>
        /// <exception cref="System.Exception">Not connected</exception>
        /// <autogeneratedoc />
        public void Broadcast(TransportPacketPriority priority, params OscPacket[] packets)
        {
            byte[] bundleBytes = GetOscPacketBytes(packets);

            lock (syncLock)
            {
                try
                {
                    foreach (ClientSession session in sessionLookup.Values)
                    {
                        if (session.Send(priority, bundleBytes, 0, bundleBytes.Length))
                        {
                            Reporter.PrintOscPackets(Direction.Transmit, session.EndPoint, packets);

                            packetStatistics.IncrementReceiveOscPackets(bundleBytes.Length);
                        }
                        else
                        {
                            packetStatistics.IncrementDroppedPackets(bundleBytes.Length);
                        }
                    }
                }
                catch
                {
                    // @TODO: find out why this is happening
                }
            }
        }

        /// <summary>
        /// Broadcast a block of data to all receivers. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="data">Byte array.</param>
        /// <param name="index">Starting index in the array.</param>
        /// <param name="count">Number of bytes to send.</param>
        /// <exception cref="System.Exception">Not connected</exception>
        /// <autogeneratedoc />
        public void Broadcast(TransportPacketPriority priority, byte[] data, int index, int count)
        {
            lock (syncLock)
            {
                foreach (ClientSession session in sessionLookup.Values)
                {
                    if (session.Send(priority, data, index, count))
                    {
                        packetStatistics.IncrementSendStreamPackets(count);
                    }
                    else
                    {
                        packetStatistics.IncrementDroppedPackets(count);
                    }
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting resources.
        /// </summary>
        /// <autogeneratedoc />
        public void Dispose()
        {
            if (State == SimulationServerState.NotListening)
            {
                return;
            }

            Stop();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <exception cref="System.Exception">
        /// A packet receiver has not been attached
        /// or
        /// A session handler has not been attached
        /// or
        /// Already listening
        /// or
        /// Could not start simulation server
        /// </exception>
        /// <autogeneratedoc />
        public void Start()
        {
            lock (syncLock)
            {
                if (receiver == null)
                {
                    throw new Exception("A packet receiver has not been attached");
                }

                if (sessionHandler == null)
                {
                    throw new Exception("A session handler has not been attached");
                }

                if (State != SimulationServerState.NotListening)
                {
                    throw new Exception("Already listening");
                }

                try
                {
                    Reporter.PrintEmphasized("Starting simulation server on {0}:{1}", Address, Port);

                    tcpListener = new TcpListener(Address, Port);

                    State = SimulationServerState.Listening;

                    tcpListener.Start();

                    Reporter.PrintNormal("The local end point is {0}", tcpListener.LocalEndpoint);

                    if (TransportSecurityProvider is OpenSecurityProvider)
                    {
                        TransportSecuritySessionToken token;
                        TransportSecurityProvider.TryAquireSession(out token);
                        Reporter.PrintNormal($"The following security token can be used: {token.SecurityKey}");
                    }

                    Reporter.PrintNormal("Waiting for a connections");

                    doneEvent.Reset();

                    acceptResult = tcpListener.BeginAcceptTcpClient(OnAccept, null);
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not start simulation server", ex);
                }
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        /// <exception cref="System.Exception">Not listening.</exception>
        /// <autogeneratedoc />
        public void Stop()
        {
            lock (syncLock)
            {
                if (State == SimulationServerState.NotListening)
                {
                    throw new Exception("Not listening.");
                }

                Reporter.PrintEmphasized("Server stopping");

                State = SimulationServerState.Closing;

                tcpListener.Stop();

                if (acceptResult != null)
                {
                    OnAccept(acceptResult);
                }

                errorEvent.Set();

                doneEvent.WaitOne();

                Reporter.PrintEmphasized("Server stopped");

                foreach (ClientSession session in sessionLookup.Values.ToArray())
                {
                    session.Stop();
                }

                sessionLookup.Clear();

                State = SimulationServerState.NotListening;

                Reporter.PrintEmphasized("All client sessions ended");
            }
        }

        /// <summary>
        /// Transmits a block of data to a single endpoint. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="packets">The packets.</param>
        /// <autogeneratedoc />
        public void Transmit(TransportPacketPriority priority, IPEndPoint endpoint, params OscPacket[] packets)
        {
            lock (syncLock)
            {
                ClientSession session;

                if (sessionLookup.TryGetValue(endpoint, out session) == false)
                {
                    return;
                }

                byte[] bundleBytes = GetOscPacketBytes(packets);

                if (session.Send(priority, bundleBytes, 0, bundleBytes.Length))
                {
                    Reporter.PrintOscPackets(Direction.Transmit, endpoint, packets);

                    packetStatistics.IncrementReceiveOscPackets(bundleBytes.Length);
                }
                else
                {
                    packetStatistics.IncrementDroppedPackets(bundleBytes.Length);
                }
            }
        }

        /// <summary>
        /// Transmits one or more <see cref="Rug.Osc.OscPacket"/> to a set of client <see cref="IPEndPoint"/>s. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="endpoints">The endpoints.</param>
        /// <param name="packets">The packets.</param>
        /// <autogeneratedoc />
        public void Transmit(TransportPacketPriority priority, IEnumerable<IPEndPoint> endpoints, params OscPacket[] packets)
        {
            byte[] bundleBytes = GetOscPacketBytes(packets);

            lock (syncLock)
            {
                foreach (IPEndPoint endpoint in endpoints)
                {
                    ClientSession session;

                    if (!sessionLookup.TryGetValue(endpoint, out session))
                    {
                        continue;
                    }

                    if (session.Send(priority, bundleBytes, 0, bundleBytes.Length))
                    {
                        Reporter.PrintOscPackets(Direction.Transmit, endpoint, packets);

                        packetStatistics.IncrementReceiveOscPackets(bundleBytes.Length);
                    }
                    else
                    {
                        packetStatistics.IncrementDroppedPackets(bundleBytes.Length);
                    }
                }
            }
        }

        /// <summary>
        /// Transmits a block of data to a set of client <see cref="IPEndPoint"/>s. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="data">Byte array.</param>
        /// <param name="index">Starting index in the array.</param>
        /// <param name="count">Number of bytes to send.</param>
        /// <param name="endpoints">Endpoints to send to.</param>
        /// <autogeneratedoc />
        public void Transmit(TransportPacketPriority priority, byte[] data, int index, int count, IEnumerable<IPEndPoint> endpoints)
        {
            lock (syncLock)
            {
                foreach (IPEndPoint endpoint in endpoints)
                {
                    ClientSession session;

                    if (!sessionLookup.TryGetValue(endpoint, out session))
                    {
                        continue;
                    }

                    if (session.Send(priority, data, index, count))
                    {
                        packetStatistics.IncrementSendStreamPackets(count);
                    }
                    else
                    {
                        packetStatistics.IncrementDroppedPackets(count);
                    }
                }
            }
        }

        bool CheckConnection()
        {
            if (IsConnected == false)
            {
                return false;
            }

            errorEvent.WaitOne();

            if (IsConnected == false)
            {
                return false;
            }

            errorEvent.Reset();

            return true;
        }

        public byte[] GetOscPacketBytes(params OscPacket[] packets)
        {
            OscBundle bundle;

            if (packets.Length == 1)
            {
                if (packets[0] is OscBundle)
                {
                    bundle = packets[0] as OscBundle;
                }
                else
                {
                    bundle = new OscBundle(DateTime.Now, packets[0] as OscMessage);
                }
            }
            else
            {
                bundle = new OscBundle(DateTime.Now, packets);
            }

            int bundleSize = bundle.SizeInBytes;

            byte[] bundleBytes = new byte[TransportPacketParser.HeaderSize + bundleSize];

            using (MemoryStream stream = new MemoryStream(bundleBytes))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write((ushort)TransportPacketType.OscBundle);
                writer.Write(bundleSize);
                writer.Write(bundle.ToByteArray());
            }

            return bundleBytes;
        }

        void OnAccept(IAsyncResult iar)
        {
            TcpClient client;

            TransportSecuritySessionToken token = null; 

            try
            {
                client = tcpListener.EndAcceptTcpClient(iar);

                Reporter.PrintEmphasized("Accept TCP client connection from {0}", client.Client.RemoteEndPoint);

                bool aquiredSessionKey = TransportSecurityProvider.TryAquireSession(out token);

                // keep listening
                acceptResult = tcpListener.BeginAcceptTcpClient(OnAccept, null);

                if (aquiredSessionKey == false)
                {
                    token?.Release(); 

                    client.Close();

                    return; 
                }
            }
            catch (ObjectDisposedException)
            {
                token?.Release(); 

                if (CheckConnection() == false)
                {
                    acceptResult = null;
                    doneEvent.Set();
                    return;
                }

                acceptResult = null;

                Reporter.PrintEmphasized("Listen canceled");

                doneEvent.Set();

                return;
            }
            catch (SocketException ex)
            {
                token?.Release();

                if (CheckConnection() == false)
                {
                    acceptResult = null;
                    doneEvent.Set();
                    return;
                }

                acceptResult = null;

                Reporter.PrintException(ex, "Error accepting TCP connection.");

                // unrecoverable
                doneEvent.Set();

                return;
            }

            // IS THIS REALLY SOME SORT OF OPTO? 
            client.NoDelay = false;            

            ClientSession session = new ClientSession(ConnectionType, token, client, packetStatistics, Reporter, receiver);

            session.Ended += Session_Ended;
            session.Started += Session_Started;

            lock (syncLock)
            {
                sessionLookup.Add(session.EndPoint, session);
            }

            session.Start();
        }

        void Session_Started(object sender, EventArgs e)
        {
            ClientSession session = sender as ClientSession;

            sessionHandler.OnSessionStarted(session.EndPoint);
        }

        void Session_Ended(object sender, EventArgs e)
        {
            ClientSession session = sender as ClientSession;

            try
            {

                if (State != SimulationServerState.Closing)
                {
                    lock (syncLock)
                    {
                        session.Stop();

                        sessionLookup.Remove(session.EndPoint);
                    }
                }
                else
                {
                    //sessionLookup.Remove(session.EndPoint);
                }

                sessionHandler.OnSessionEnded(session.EndPoint);
            }
            finally
            {
                session.SessionToken?.Release();
            }
        }

        /// <summary>
        /// Stop all client sessions
        /// </summary>
        public void StopAllClientSessions()
        {
            lock (syncLock)
            {
                Reporter.PrintEmphasized("Stop all client sessions");

                foreach (ClientSession session in sessionLookup.Values.ToArray())
                {
                    session.Stop();
                }
            }
        }

        /// <summary>
        /// Flushes the packet if packet buffering is enabled.
        /// </summary>
        public void Flush()
        {
            lock (syncLock)
            {
                try
                {
                    foreach (ClientSession session in sessionLookup.Values)
                    {
                        session.Flush();
                    }
                }
                catch (Exception e)
                {
                    Reporter?.PrintException(e, "Exception thrown attempting to flush session.");
                }
            }
        }
    }
}