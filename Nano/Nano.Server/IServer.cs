﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Loading;

namespace Nano.Server
{
    public interface IServer : ILoadable, IDisposable
    {
        string Description { get; } 
        
        string Name { get; } 
        
        IDiscovery Discovery { get; }

        IAssetManager AssetManager { get; } 

        IMonitor Monitor { get; }
        
        IReportManager ReportManager { get; }
        
        IService Service { get; }

        ISecurityProvider SecurityProvider { get; }
    }
}