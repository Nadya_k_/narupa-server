﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Threading;
using System.Threading.Tasks;
using Nano.Transport.Comms.Security;

namespace Nano.Server
{
    public interface ISecurityProvider : IServerComponent
    {
        ITransportSecurityProvider TransportSecurityProvider { get; }

        void Initialize(IServer serverContext, IReporter reporter);
        
        Task Start(IServer serverContext, CancellationToken cancel);               
    }
}