﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;
using Rug.Osc;
using Rug.Osc.Ahoy;

namespace Nano.Server.Basic
{
    [XmlName("AhoyDiscovery")]
    public class AhoyDiscovery : IDiscovery
    {
//        public AhoyServiceProvider AhoyService { get; private set; }
//
//        public AhoyService Service { get; }           
                
        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            return element; 
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            
        }

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            IReporter reporter = serverContext.ReportManager.BeginReport(ReportContext.Discovery, "ahoy", out Uri logIdentifier);            
            
            AhoyServiceProvider ahoyService = new AhoyServiceProvider(IPAddress.Any);

            void OnAhoyServiceOnMessageReceived(OscMessage message) => reporter?.PrintOscPackets(Direction.Receive, message.Origin, message);
            void OnAhoyServiceOnMessageSent(OscMessage message) => reporter?.PrintOscPackets(Direction.Transmit, message.Origin, message);

            try
            {                
                IServiceEndpoint serviceEndpoint = serverContext.Service.Endpoint ?? throw new NullReferenceException("Cannot start ahoy as no service endpoint has been defined");                
                
                // resolve service information here
                ahoyService.Add(new AhoyService(serverContext.Name, serviceEndpoint.EndpointUri.Port, serviceEndpoint.EndpointUri.Port));

                ahoyService.MessageReceived += OnAhoyServiceOnMessageReceived;
                ahoyService.MessageSent += OnAhoyServiceOnMessageSent;

                ahoyService.Start();
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "ahoy started");

                do
                {
                    await Task.Delay(60000, cancel); 
                }
                while (cancel.IsCancellationRequested == false); 
            }
            catch (TaskCanceledException _)
            {
                reporter.PrintDebug(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "ahoy task canceled");
            }
            catch (Exception ex)
            {
                reporter.PrintException(ex, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Exception in ahoy discovery task");
            }
            finally
            {                                
                ahoyService.MessageReceived -= OnAhoyServiceOnMessageReceived;
                ahoyService.MessageSent -= OnAhoyServiceOnMessageSent;
                
                ahoyService.Dispose();
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "ahoy ended");

                serverContext.ReportManager.EndReport(logIdentifier);
            }
        }
    }
}