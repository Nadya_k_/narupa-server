﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Nano.Transport.Comms.Security;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("OpenSecurityProvider")]
    public class OpenSecurityProvider : ISecurityProvider
    {
        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            return element;
        }

        /// <inheritdoc />
        public void Initialize(IServer serverContext, IReporter reporter)
        {
            TransportSecurityProvider = new Nano.Transport.Comms.Security.OpenSecurityProvider();
        }

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            IReporter reporter = serverContext.ReportManager.BeginReport(ReportContext.Security, "open-security-provider", out Uri reportIdentifier);

            try
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "open-security-provider started");

                do
                {
                    await Task.Delay(60000, cancel); 
                }
                while (cancel.IsCancellationRequested == false); 
            }
            catch (TaskCanceledException _)
            {
                reporter.PrintDebug(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "open-security-provider task canceled");
            }
            finally
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "open-security-provider ended");

                serverContext.ReportManager.EndReport(reportIdentifier);
            }
        }

        /// <inheritdoc />
        public ITransportSecurityProvider TransportSecurityProvider { get; private set; }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
        }
    }
}