﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow.Private.v1_0
{
    public enum ConnectionSecurityType
    {
        Open = 0,
        Reservation = 1
    }
}