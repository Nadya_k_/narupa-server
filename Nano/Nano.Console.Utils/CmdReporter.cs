﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Net;
using Rug.Osc;

namespace Nano.Console.Utils
{
    public class CmdReporter : IReporter
    {
        private readonly object syncLock = new object();

        public IOscMessageFilter OscMessageFilter { get; set; }

        public ReportVerbosity ReportVerbosity { get; set; }

        public void PrintBlankLine(ReportVerbosity verbosity)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteLine(Colors.Emphasized, "");
            }
        }

        public void PrintDebug(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Debug, Colors.Debug);
        }

        public void PrintDetail(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintEmphasized(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Emphasized, Colors.Emphasized);
        }

        public void PrintError(string format, params object[] args)
        {
            lock (syncLock)
            {
                Cmd.WriteLine(Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, IPEndPoint origin, string format, params object[] args)
        {
            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, origin.ToString(), Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, ident, Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintException(Exception ex, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (ShouldPrint(ReportVerbosity.Detail) == false)
                {
                    Cmd.WriteLine(Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
                }
                else
                {
                    Cmd.WriteException(args.Length == 0 ? format : string.Format(format, args), ex);
                }
            }
        }

        /// <inheritdoc />
        public void PrintHeading(string format, params object[] args)
        {
            if (ShouldPrint(ReportVerbosity.Emphasized) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteLine(Colors.Heading, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintNormal(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                if (packet is OscMessage)
                {
                    PrintOscMessage(direction, packet as OscMessage);
                }
                else if (packet is OscBundle)
                {
                    OscBundle bundle = packet as OscBundle;

                    foreach (OscPacket sub in bundle)
                    {
                        PrintOscPackets(direction, sub);
                    }
                }
            }
        }

        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                if (packet is OscMessage)
                {
                    PrintOscMessage(direction, endPoint, packet as OscMessage);
                }
                else if (packet is OscBundle)
                {
                    OscBundle bundle = packet as OscBundle;

                    foreach (OscPacket sub in bundle)
                    {
                        PrintOscPackets(direction, endPoint, sub);
                    }
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteLine(Colors.Warning, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Warning, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, ident, Colors.Warning, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColor color)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(Direction.Action, Colors.Ident, "", color, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(Direction direction, string ident, string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColor color)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, ident, color, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void PrintOscMessage(Direction direction, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, oscMessage.Origin.ToString(), Colors.Message, oscMessage.ToString());
            }
        }

        private void PrintOscMessage(Direction direction, IPEndPoint endPoint, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Cmd.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Message, oscMessage.ToString());
            }
        }

        private bool ShouldPrint(ReportVerbosity verbosity)
        {
            return (int)ReportVerbosity <= (int)verbosity;
        }
    }
}