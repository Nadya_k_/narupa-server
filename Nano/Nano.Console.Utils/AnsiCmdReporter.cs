// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Net;
using Rug.Osc;

namespace Nano.Console.Utils
{
    public class AnsiCmdReporter : IReporter
    {
        private readonly object syncLock = new object();
        
        public static bool InsetActions { get; set; } = true; 
        
        public IOscMessageFilter OscMessageFilter { get; set; }

        public ReportVerbosity ReportVerbosity { get; set; }

        public void PrintBlankLine(ReportVerbosity verbosity)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteLine(Colors.Emphasized, AnsiCmd.TextFormatting.Normal, "");
            }
        }

        public void PrintDebug(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Debug, Colors.Debug, AnsiCmd.TextFormatting.Italic);
        }

        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Debug, Colors.Debug, AnsiCmd.TextFormatting.Italic);
        }

        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Debug, Colors.Debug, AnsiCmd.TextFormatting.Italic);
        }

        public void PrintDetail(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Detail, Colors.Normal);
        }

        public void PrintEmphasized(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Emphasized, Colors.Emphasized, AnsiCmd.TextFormatting.Bold);
        }

        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Emphasized, Colors.Emphasized, AnsiCmd.TextFormatting.Bold);
        }

        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Emphasized, Colors.Emphasized, AnsiCmd.TextFormatting.Bold);
        }

        public void PrintError(string format, params object[] args)
        {
            lock (syncLock)
            {
                AnsiCmd.Error.WriteLine(Colors.ErrorDetail, AnsiCmd.TextFormatting.Bold, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, IPEndPoint origin, string format, params object[] args)
        {
            lock (syncLock)
            {
                AnsiCmd.Error.WriteMessage(direction, Colors.Ident, origin.ToString(), Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
            lock (syncLock)
            {
                AnsiCmd.Error.WriteMessage(direction, Colors.Ident, ident, Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintException(Exception ex, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (ShouldPrint(ReportVerbosity.Normal) == false)
                {
                    AnsiCmd.Error.WriteLine(Colors.ErrorDetail, args.Length == 0 ? format : string.Format(format, args));
                }
                else
                {
                    AnsiCmd.Error.WriteException(args.Length == 0 ? format : string.Format(format, args), ex, ShouldPrint(ReportVerbosity.Detail));
                }
            }
        }

        /// <inheritdoc />
        public void PrintHeading(string format, params object[] args)
        {
            if (ShouldPrint(ReportVerbosity.Emphasized) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteLine(Colors.Heading, AnsiCmd.TextFormatting.Bold, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintNormal(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Normal, Colors.Normal);
        }

        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                switch (packet)
                {
                    case OscMessage oscMessage:
                        PrintOscMessage(direction, oscMessage);
                        break;

                    case OscBundle bundle:
                        foreach (OscPacket sub in bundle)
                        {
                            PrintOscPackets(direction, sub);
                        }
                        break;
                }
            }
        }

        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                switch (packet)
                {
                    case OscMessage oscMessage:
                        PrintOscMessage(direction, endPoint, oscMessage);
                        break;

                    case OscBundle bundle:
                        foreach (OscPacket sub in bundle)
                        {
                            PrintOscPackets(direction, endPoint, sub);
                        }
                        break;
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteLine(Colors.Warning, AnsiCmd.TextFormatting.Italic, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Warning, AnsiCmd.TextFormatting.Italic, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(direction, Colors.Ident, ident, Colors.Warning, AnsiCmd.TextFormatting.Italic, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColor color, AnsiCmd.TextFormatting formatting = AnsiCmd.TextFormatting.Normal)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(Direction.Action, Colors.Ident, "", color, formatting, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(Direction direction, string ident, string format, object[] args, ReportVerbosity reportVerbosity, ConsoleColor color, AnsiCmd.TextFormatting formatting = AnsiCmd.TextFormatting.Normal)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(direction, Colors.Ident, ident, color, formatting, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void PrintOscMessage(Direction direction, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(direction, Colors.Ident, oscMessage.Origin.ToString(), Colors.Message, oscMessage.ToString());
            }
        }

        private void PrintOscMessage(Direction direction, IPEndPoint endPoint, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                AnsiCmd.Out.WriteMessage(direction, Colors.Ident, endPoint.ToString(), Colors.Message, oscMessage.ToString());
            }
        }

        private bool ShouldPrint(ReportVerbosity verbosity)
        {
            return (int)ReportVerbosity <= (int)verbosity;
        }
    }
}