﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using System;
using System.Net;

namespace Nano.Client
{
    public sealed class CloudConnectionInfo : SimboxConnectionInfo
    {
        public override IPAddress Address => ConnectionString.IPAddress;

        public override ConnectionString ConnectionString { get; }
        public override SimboxConnectionType ConnectionType => SimboxConnectionType.Cloud;

        public string SessionKey { get; }
        public override int Port => ConnectionString.Port;

        public CloudConnectionInfo(string descriptor, Uri uri, string sessionKey) : base(descriptor)
        {
            ConnectionString = new ConnectionString(uri);

            SessionKey = sessionKey;
        }

        public override bool Equals(SimboxConnectionInfo other)
        {
            if ((other is CloudConnectionInfo) == false)
            {
                return false;
            }

            CloudConnectionInfo otherCloudConnectionInfo = (CloudConnectionInfo) other;

            return
                ConnectionType == otherCloudConnectionInfo.ConnectionType &&
                Descriptor.Equals(otherCloudConnectionInfo.Descriptor) &&
                ConnectionString.Equals(otherCloudConnectionInfo.ConnectionString);
        }
    }
}