﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Variables;
using NUnit.Framework;
using SlimMath;

namespace Nano.Tests.Variables
{
    [TestFixture()]
    public class VariableTypesTest
    {
        [Test]
        public void GetVariableType_Type_Bool()
        {
            Assert.AreEqual(VariableType.Bool, VariableTypes.GetVariableType(typeof(bool)));
        }

        [Test]
        public void GetVariableType_Type_Int8()
        {
            Assert.AreEqual(VariableType.Int8, VariableTypes.GetVariableType(typeof(sbyte)));
        }

        [Test]
        public void GetVariableType_Type_UInt8()
        {
            Assert.AreEqual(VariableType.UInt8, VariableTypes.GetVariableType(typeof(byte)));
        }

        [Test]
        public void GetVariableType_Type_Int16()
        {
            Assert.AreEqual(VariableType.Int16, VariableTypes.GetVariableType(typeof(short)));
        }

        [Test]
        public void GetVariableType_Type_UInt16()
        {
            Assert.AreEqual(VariableType.UInt16, VariableTypes.GetVariableType(typeof(ushort)));
        }

        [Test]
        public void GetVariableType_Type_Int32()
        {
            Assert.AreEqual(VariableType.Int32, VariableTypes.GetVariableType(typeof(int)));
        }

        [Test]
        public void GetVariableType_Type_UInt32()
        {
            Assert.AreEqual(VariableType.UInt32, VariableTypes.GetVariableType(typeof(uint)));
        }

        [Test]
        public void GetVariableType_Type_Int64()
        {
            Assert.AreEqual(VariableType.Int64, VariableTypes.GetVariableType(typeof(long)));
        }

        [Test]
        public void GetVariableType_Type_UInt64()
        {
            Assert.AreEqual(VariableType.UInt64, VariableTypes.GetVariableType(typeof(ulong)));
        }


        [Test]
        public void GetVariableType_Type_Half()
        {
            Assert.AreEqual(VariableType.Half, VariableTypes.GetVariableType(typeof(Half)));
        }

        [Test]
        public void GetVariableType_Type_Single()
        {
            Assert.AreEqual(VariableType.Float, VariableTypes.GetVariableType(typeof(float)));
        }

        [Test]
        public void GetVariableType_Type_Double()
        {
            Assert.AreEqual(VariableType.Double, VariableTypes.GetVariableType(typeof(double)));
        }


        [Test]
        public void GetVariableType_Type_String()
        {
            Assert.AreEqual(VariableType.String, VariableTypes.GetVariableType(typeof(string)));
        }

        [Test]
        public void GetVariableType_Type_Unknown()
        {
            Assert.AreEqual(VariableType.Unknown, VariableTypes.GetVariableType(typeof(object)));
        }




        [Test]
        public void GetVariableType_Object_Bool()
        {
            Assert.AreEqual(VariableType.Bool, VariableTypes.GetVariableType(true));
            Assert.AreEqual(VariableType.Bool, VariableTypes.GetVariableType(false));
            Assert.AreEqual(VariableType.Bool, VariableTypes.GetVariableType((object)true));
            Assert.AreEqual(VariableType.Bool, VariableTypes.GetVariableType((object)false));
        }

        [Test]
        public void GetVariableType_Object_Int8()
        {
            Assert.AreEqual(VariableType.Int8, VariableTypes.GetVariableType((sbyte)64));
            Assert.AreEqual(VariableType.Int8, VariableTypes.GetVariableType((object)((sbyte)64)));
        }

        [Test]
        public void GetVariableType_Object_UInt8()
        {
            Assert.AreEqual(VariableType.UInt8, VariableTypes.GetVariableType((byte)64));
            Assert.AreEqual(VariableType.UInt8, VariableTypes.GetVariableType((object)((byte)64)));
        }

        [Test]
        public void GetVariableType_Object_Int16()
        {
            Assert.AreEqual(VariableType.Int16, VariableTypes.GetVariableType((short)64));
            Assert.AreEqual(VariableType.Int16, VariableTypes.GetVariableType((object)((short)64)));
        }

        [Test]
        public void GetVariableType_Object_UInt16()
        {
            Assert.AreEqual(VariableType.UInt16, VariableTypes.GetVariableType((ushort)64));
            Assert.AreEqual(VariableType.UInt16, VariableTypes.GetVariableType((object)((ushort)64)));            
        }

        [Test]
        public void GetVariableType_Object_Int32()
        {
            Assert.AreEqual(VariableType.Int32, VariableTypes.GetVariableType((int)64));
            Assert.AreEqual(VariableType.Int32, VariableTypes.GetVariableType((object)((int)64)));            
        }

        [Test]
        public void GetVariableType_Object_UInt32()
        {
            Assert.AreEqual(VariableType.UInt32, VariableTypes.GetVariableType((uint)64));
            Assert.AreEqual(VariableType.UInt32, VariableTypes.GetVariableType((object)((uint)64)));
        }

        [Test]
        public void GetVariableType_Object_Int64()
        {
            Assert.AreEqual(VariableType.Int64, VariableTypes.GetVariableType((long)64));
            Assert.AreEqual(VariableType.Int64, VariableTypes.GetVariableType((object)((long)64)));
        }

        [Test]
        public void GetVariableType_Object_UInt64()
        {
            Assert.AreEqual(VariableType.UInt64, VariableTypes.GetVariableType((ulong)64));
            Assert.AreEqual(VariableType.UInt64, VariableTypes.GetVariableType((object)((ulong)64)));            
        }


        [Test]
        public void GetVariableType_Object_Half()
        {
            Assert.AreEqual(VariableType.Half, VariableTypes.GetVariableType((Half)1f));
            Assert.AreEqual(VariableType.Half, VariableTypes.GetVariableType((object)((Half)1f)));
        }

        [Test]
        public void GetVariableType_Object_Single()
        {
            Assert.AreEqual(VariableType.Float, VariableTypes.GetVariableType((float)1f));
            Assert.AreEqual(VariableType.Float, VariableTypes.GetVariableType((object)((float)1f)));            
        }

        [Test]
        public void GetVariableType_Object_Double()
        {
            Assert.AreEqual(VariableType.Double, VariableTypes.GetVariableType((double)1f));
            Assert.AreEqual(VariableType.Double, VariableTypes.GetVariableType((object)((double)1f)));
        }


        [Test]
        public void GetVariableType_Object_String()
        {
            Assert.AreEqual(VariableType.String, VariableTypes.GetVariableType("TEST STRING"));
            Assert.AreEqual(VariableType.String, VariableTypes.GetVariableType((object)"TEST STRING"));
        }

        [Test]
        public void GetVariableType_Object_Vector2()
        {
            Assert.AreEqual(VariableType.Vector2, VariableTypes.GetVariableType((Vector2)new Vector2(23, 85)));
            Assert.AreEqual(VariableType.Vector2, VariableTypes.GetVariableType((object)(new Vector2(23, 85))));
        }

        [Test]
        public void GetVariableType_Object_Vector3()
        {
            Assert.AreEqual(VariableType.Vector3, VariableTypes.GetVariableType((Vector3)new Vector3(23, 85, -64)));
            Assert.AreEqual(VariableType.Vector3, VariableTypes.GetVariableType((object)(new Vector3(23, 85, -64))));
        }

        [Test]
        public void GetVariableType_Object_BoundingBox()
        {
            Assert.AreEqual(VariableType.BoundingBox, VariableTypes.GetVariableType((BoundingBox)new BoundingBox(new Vector3(-23, -85, -64), new Vector3(23, 85, 64))));
            Assert.AreEqual(VariableType.BoundingBox, VariableTypes.GetVariableType((object)new BoundingBox(new Vector3(-23, -85, -64), new Vector3(23, 85, 64))));
        }

        [Test]
        public void GetVariableType_Object_Unknown()
        {
            Assert.AreEqual(VariableType.Unknown, VariableTypes.GetVariableType(typeof(object)));
        }
    }
}