﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Science.Simulation;
using NUnit.Framework;
using SlimMath;

namespace SimboxDynamics.System.Tests
{
    [TestFixture()]
    public class ParticleCollectionTests
    {
        private ParticleCollection CreateTestParticleCollection()
        {
            ParticleCollection collection = new ParticleCollection();

            for (int i = 0; i < 10; i++)
            {
                collection.AddParticle(i, new Vector3(i), new Vector3(2 * i), new Vector3(3 * i), i % 3 == 0);
            }
            return collection;
        }

        [Test()]
        public void ZeroVelocitiesTest()
        {
            ParticleCollection collection = CreateTestParticleCollection();

            collection.ZeroVelocities();

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(SlimMath.Vector3.Zero, collection.Velocity[i]);
            }
        }

        [Test()]
        public void AddParticleTest()
        {
            ParticleCollection collection = new ParticleCollection();
            collection.AddParticle(1f, new SlimMath.Vector3(0f), new SlimMath.Vector3(0f), new SlimMath.Vector3(0f));
        }

        [Test()]
        public void AddParticleExceptionTest()
        {
            ParticleCollection collection = new ParticleCollection();
            try
            {
                collection.AddParticle(-1f, new SlimMath.Vector3(0f), new SlimMath.Vector3(0f), new SlimMath.Vector3(0f));
                Assert.Fail();
            }
            catch
            {
            }
        }

        [Test()]
        public void ZeroForcesTest()
        {
            ParticleCollection collection = CreateTestParticleCollection();

            collection.ZeroForces();

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(SlimMath.Vector3.Zero, collection.Force[i]);
            }
        }

        [Test()]
        public void ParticleCollectionCloneTest()
        {
            ParticleCollection p1 = CreateTestParticleCollection();
            ParticleCollection clone = p1.Clone();

            //Check result is equal.
            Assert.IsTrue(p1.Equals(clone));

            //Make a change to p1 and check no longer equal.
            p1.ZeroForces();

            Assert.IsFalse(p1.Equals(clone));
        }

        [Test()]
        public void RemoveCentreOfMassMotionTest()
        {
            ParticleCollection p = new ParticleCollection();

            for (int i = 0; i < p.NumberOfParticles; i++)
            {
                p.AddParticle(1, new Vector3(i), new Vector3(1), Vector3.Zero);
            }

            p.RemoveCentreOfMassMotion();

            for (int i = 0; i < p.NumberOfParticles; i++)
            {
                Assert.AreEqual(Vector3.Zero, p.Velocity[i]);
            }
        }

        [Test()]
        public void ParticleCollectionEqualsTest()
        {
            ParticleCollection p1 = CreateTestParticleCollection();
            ParticleCollection p2 = CreateTestParticleCollection();

            Assert.IsTrue(p1.Equals(p2));
        }

        [Test()]
        public void ParticleCollectionNotEqualsTest()
        {
            ParticleCollection p1 = CreateTestParticleCollection();
            ParticleCollection p2 = CreateTestParticleCollection();

            Vector3 f = p2.Force[2];
            p2.Force[2] = SlimMath.Vector3.Zero;
            Assert.IsFalse(p1.Equals(p2));
            p2.Force[0] = f;

            p2.Velocity[2] = SlimMath.Vector3.Zero;
            Assert.IsFalse(p1.Equals(p2));

            p2 = CreateTestParticleCollection();

            p2.RemoveParticle(0);
            Assert.IsFalse(p1.Equals(p2));
        }
    }
}