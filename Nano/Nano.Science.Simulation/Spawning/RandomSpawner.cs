﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Nano.Science.Simulation.Spawning
{
    /// <summary>
    /// Spawns a number of this template in random positions and oritentations in the simulation box, subject to them not overlapping 
    /// with existing templates.
    /// </summary>
    [XmlName("Spawner")]
    public sealed class RandomSpawner : ISpawner, ILoadable
    {
        /// <inheritdoc />
        public IEnumerable<string> Templates { get { return new string[] { Template }; } }

        /// <inheritdoc />
        public string Name { get; set; }

        /// <summary>
        /// Template to spawn.
        /// </summary>
        public string Template;

        /// <summary>
        /// Where template will be spawned.
        /// </summary>
        public Vector3 Origin;

        /// <summary>
        /// Rotation to apply to template.
        /// </summary>
        public Quaternion Rotation;

        /// <summary>
        /// Number of template.
        /// </summary>
        public int Count;

        /// <inheritdoc />
        public RandomSpawner()
        {
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", null);

            if (string.IsNullOrEmpty(Name) == true)
            {
                throw new Exception("Missing 'Name' attribute.");
            }

            Template = Helper.GetAttributeValue(node, "Template", null);

            Origin = Helper.GetAttributeValue(node, "Origin", Vector3.Zero);
            var rotationEuler = Helper.GetAttributeValue(node, "Rotation", Vector3.Zero);

            Rotation = MathUtilities.QuaternionFromEuler(rotationEuler, radians: false);
            Count = Helper.GetAttributeValue(node, "Count", Count);
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            Helper.AppendAttributeAndValue(element, "Template", Template);
            Helper.AppendAttributeAndValue(element, "Origin", Origin);
            Helper.AppendAttributeAndValue(element, "Rotation", MathUtilities.EulerFromQuaternion(Rotation));

            Helper.AppendAttributeAndValue(element, "Count", Count);

            return element;
        }

        /// <summary>
        /// Spawns the specified topology.
        /// </summary>
        /// <param name="box">The simulation bounding box.</param>
        /// <param name="boundingSpheres">The bounding spheres.</param>
        /// <param name="topology">The topology.</param>
        /// <param name="parentContext">The parent context.</param>
        /// <param name="spawnables">The spawnables.</param>
        /// <exception cref="System.Exception"></exception>
        public void Spawn(ref ISimulationBoundary box, ref List<BoundingSphere> boundingSpheres, InstantiatedTopology topology, ref SpawnContext parentContext, params ISpawnable[] spawnables)
        {
            BoundingBox simulationBox = box.SimulationBox;

            //For each spawnable residue, attempt to place it randomly so it does not overlap with other residues.
            foreach (ISpawnable spawnable in spawnables)
            {
                string baseAddress = parentContext.GetBaseAddress(spawnable.Name);

                // string baseAddress = string.Format("{0}/{1}_", parentContext.Address, spawnable.Name);

                SpawnContext context = new SpawnContext()
                {
                    Index = 0,
                    Reporter = parentContext.Reporter
                };

                for (int i = 0; i < Count; i++)
                {
                    bool overlap = true;

                    int maxIterations = 100000;
                    int iteration = 0;

                    context.Rotation = MathUtilities.GenerateRandomQuarternion() * parentContext.Rotation;

                    while (iteration++ < maxIterations)
                    {
                        //TODO should include the parents offset?
                        context.Offset = SpawnHelper.RandomOffset(ref simulationBox);

                        //Get the bounding box of the spawnable and all its children at the given offset.
                        BoundingBox spawnBox = spawnable.GetBoundingBox(ref context);
                        //Check the box does not go over the boundary.
                        if (SpawnHelper.BoxContainsBox(simulationBox, spawnBox) == false)
                            continue;

                        if (spawnable.Intersects(boundingSpheres, ref context) == false)
                        {
                            overlap = false;

                            break;
                        }
                    }

                    if (overlap)
                    {
                        throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, string.Format("Could not find a location to spawn the residue \"{0}\" in the box without it overlapping with other residues. Try making the simulation box bigger, and check that the residue initial positions are correct.", baseAddress));
                    }

                    //context.Address = baseAddress + string.Format("{0:X4}", context.Index++);
                    context.Address = parentContext.GetAddress(Name, context.Index++);

                    spawnable.Spawn(box, boundingSpheres, topology, ref context);
                }
            }
        }
    }
}