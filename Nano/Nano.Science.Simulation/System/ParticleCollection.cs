﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using SlimMath;
using System;
using System.Collections.Generic;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a collection of particles.
    /// </summary>
    /// <remarks>Data structures stored in Structure of Arrays (SoA) format.</remarks>
    public class ParticleCollection : IEquatable<ParticleCollection>
    {
        #region Public Fields

        /// <summary> List of Cartesian forces acting on atoms.</summary>
        public List<Vector3> Force;

        /// <summary> List of masses of atoms.</summary>
        public List<float> Mass;

        /// <summary> List of  Cartesian atomic positions in Angstroms.</summary>
        //TODO Change these to internal, and give direct access to stream.
        public List<Vector3> Position;

        /// <summary> List of Cartesian velocities.</summary>
        public List<Vector3> Velocity;

        /// <summary> List of whether particle is virtual.</summary>
        public List<bool> IsVirtual;

        /// <summary>
        /// The number of virtual particles in the system.
        /// </summary>
        public int NumberOfVirtualParticles;

        private TransportContext transportContext;

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticleCollection"/> class.
        /// </summary>
        /// <param name="numberOfParticles">The number of particles.</param>
        public ParticleCollection(int numberOfParticles = 0)
        {
            Position = new List<Vector3>(numberOfParticles);
            Force = new List<Vector3>(numberOfParticles);
            Velocity = new List<Vector3>(numberOfParticles);
            Mass = new List<float>(numberOfParticles);
            IsVirtual = new List<bool>(numberOfParticles);
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the number of particles.
        /// </summary>
        /// <remarks>
        /// This includes virtual particles, and so should not be used in calculations where virtual particles do not want to be counted.
        /// </remarks>
        /// <value>The number of particles.</value>
        public int NumberOfParticles { get { return Position.Count; } }

        /// <summary>
        /// Gets the number of non virtual particles.
        /// </summary>
        /// <value>The number of non virtual particles.</value>
        public int NumberOfNonVirtualParticles { get { return NumberOfParticles - NumberOfVirtualParticles; } }

        #endregion Public Properties

        /// <summary>
        /// Adds the particle to the collection.
        /// </summary>
        /// <param name="mass">The mass.</param>
        /// <param name="position">The position.</param>
        /// <param name="velocity">The velocity.</param>
        /// <param name="force">The force.</param>
        /// <param name="isVirtual">if set to <c>true</c> [is virtual particle].</param>
        /// <exception cref="Exception">Cannot create a particle of negative mass!</exception>
        /// <remarks>Adding a particle here does not add it to force fields.</remarks>
        public void AddParticle(float mass, Vector3 position, Vector3 velocity, Vector3 force, bool isVirtual = false)
        {
            //Basic range checking
            if (mass < 0f)
            {
                throw new Exception("Cannot create a particle of negative mass!");
            }
            Position.Add(position);
            Velocity.Add(velocity);
            Force.Add(force);
            IsVirtual.Add(isVirtual);
            if (isVirtual)
            {
                mass = 0f;
                NumberOfVirtualParticles++;
            }
            Mass.Add(mass);
        }

        internal void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        internal void DetachTransportContext(TransportContext transportContext)
        {
        }

        /// <summary>
        /// Removes the particle at the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        public void RemoveParticle(int index)
        {
            Position.RemoveAt(index);
            Velocity.RemoveAt(index);
            Force.RemoveAt(index);
            if (IsVirtual[index])
                NumberOfVirtualParticles--;
            IsVirtual.RemoveAt(index);
            Mass.RemoveAt(index);
        }

        /// <summary>
        /// Zeroes the velocities.
        /// </summary>
        public void ZeroVelocities()
        {
            for (int i = 0; i < NumberOfParticles; i++)
            {
                Velocity[i] = Vector3.Zero;
            }
        }

        /// <summary>
        /// Sets the Cartesian forces of system to zero
        /// </summary>
        public void ZeroForces()
        {
            for (int i = 0; i < Force.Count; i++)
            {
                Force[i] = Vector3.Zero;
            }
        }

        /// <summary>
        /// Rescales the velocities.
        /// </summary>
        /// <param name="factor">The factor.</param>
        public void RescaleVelocities(float factor)
        {
            for (int i = 0; i < Velocity.Count; i++)
            {
                Velocity[i] *= factor;
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>ParticleCollection.</returns>
        public ParticleCollection Clone()
        {
            ParticleCollection p = new ParticleCollection();
            p.Position = new List<Vector3>(Position);
            p.Velocity = new List<Vector3>(Velocity);
            p.Mass = new List<float>(Mass);
            p.Force = new List<Vector3>(Force);
            p.IsVirtual = new List<bool>(IsVirtual);
            p.NumberOfVirtualParticles = NumberOfVirtualParticles;
            return p;
        }

        internal void PopParticle()
        {
            int index = NumberOfParticles - 1;
            RemoveParticle(index); ;
        }

        private Vector3 GetCentreOfMassVelocity()
        {
            Vector3 cmv = Vector3.Zero;
            float totalMass = 0f;
            for (int i = 0; i < NumberOfParticles; i++)
            {
                cmv += Velocity[i] * Mass[i];
                totalMass += Mass[i];
            }
            return cmv / totalMass;
        }

        /// <summary>
        /// Removes the centre of mass motion from velocity.
        /// </summary>
        public void RemoveCentreOfMassMotion()
        {
            Vector3 cmv = GetCentreOfMassVelocity();
            for (int i = 0; i < NumberOfParticles; i++)
            {
                Velocity[i] -= cmv;
            }
        }

        /// <inheritdoc />
        public bool Equals(ParticleCollection other)
        {
            if (other == null) return false;
            if (other.NumberOfParticles != NumberOfParticles) return false;
            if (other.NumberOfNonVirtualParticles != NumberOfNonVirtualParticles) return false;
            if (other.NumberOfVirtualParticles != NumberOfVirtualParticles) return false;
            for (int i = 0; i < NumberOfParticles; i++)
            {
                if (other.IsVirtual[i] != IsVirtual[i]) return false;
                if (Math.Abs(other.Mass[i] - Mass[i]) > 0.01f) return false;
                if (other.Force[i] != Force[i]) return false;
                if (other.Position[i] != Position[i]) return false;
                if (other.Velocity[i] != Velocity[i]) return false;
            }
            return true;
        }
    }
}