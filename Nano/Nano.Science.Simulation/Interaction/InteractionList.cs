﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using SlimMath;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a list of interactions.
    /// </summary>
    public class InteractionList : List<Interaction>
    {
        private List<Interaction> newInteractions = new List<Interaction>();
        private List<Interaction> updatedInteractions = new List<Interaction>();

        /// <summary>
        /// Replaces current interactions with new ones.
        /// </summary>
        /// <param name="interactions"></param>
        public void SetInteractions(List<Interaction> interactions)
        {
            if (interactions == null)
                return;
            updatedInteractions.Clear();
            newInteractions.Clear();
            //Loop over all interactions, and update position of existing interaction if they are the same.
            foreach (Interaction possibleNew in interactions)
            {
                bool foundMatchingInteraction = false;
                if (possibleNew.SelectedAtoms.Count == 0)
                {
                    foreach (var currentInteraction in this)
                    {
                        if (IsSameInteraction(currentInteraction, possibleNew))
                        {
                            foundMatchingInteraction = true;
                            currentInteraction.Position = possibleNew.Position;
                            updatedInteractions.Add(currentInteraction);
                            break;
                        }
                    }
                }
                if (foundMatchingInteraction == false)
                    newInteractions.Add(possibleNew);
            }
            //Clear current list of interactions (getting rid of any old ones), and add the updated and totally new ones.
            Clear();
            AddRange(updatedInteractions);
            AddRange(newInteractions);
        }

        /// <summary>
        /// Determines whether two interaction points are likely to be one continuous interaction.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="cutOffDistance"> Cutoff distance beyond which it is assumed interactions are different. </param>
        /// <returns> True if the interactions share the same playerID, inputID and the distance between interactions is less than the cutOffDistance. </returns>
        private bool IsSameInteraction(Interaction a, Interaction b, float cutOffDistance = 1.0f)
        {
            if (a.PlayerId == b.PlayerId)
            {
                if (a.InputId == b.InputId)
                {
                    if (Vector3.Distance(a.Position, b.Position) < cutOffDistance)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}