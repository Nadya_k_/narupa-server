﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using SlimMath;

namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Implementer of molecular constraint algorithm.
    /// </summary>
    public interface IConstraintImplementation
    {
        /// <summary>
        /// Applies the constraints to positions.
        /// </summary>
        /// <param name="positions">The positions.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <param name="candidatePositions">The candidate positions the constraint algorithm should adjust.</param>
        void ApplyConstraintsToPositions(List<Vector3> positions, List<Vector3> candidatePositions, List<float> inverseMasses, float tolerance);

        /// <summary>
        /// Applies the constraints to velocities.
        /// </summary>
        /// <param name="velocities">The velocities.</param>
        /// <param name="tolerance">The tolerance.</param>
        ///         /// <param name="candidateVelocities">The candidate positions the constraint algorithm should adjust.</param>
        void ApplyConstraintsToVelocities(List<Vector3> velocities, List<Vector3> candidateVelocities, List<float> inverseMasses, float tolerance);

        /// <summary>
        /// Determines whether this instance has constraints that need to be enforced.
        /// </summary>
        /// <returns><c>true</c> if this instance has constraints; otherwise, <c>false</c>.</returns>
        bool HasConstraints();

        /// <summary>
        /// Number of constraints.
        /// </summary>
        /// <returns>Number of constraints.</returns>
        int GetNumberOfConstraints();

        IEnumerable<Constraint> GetConstraints();
    }
}