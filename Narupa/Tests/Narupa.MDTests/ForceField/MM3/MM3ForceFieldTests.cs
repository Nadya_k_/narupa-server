﻿using System;
using System.Collections.Generic;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Narupa.MD.ForceField.MM3;
using Narupa.MD.ForceField.MM3.Load;
using Narupa.MD.System;
using NUnit.Framework;
using SlimMath;

namespace Narupa.MD.Tests.ForceField.MM3
{
    [TestFixture()]
    public class MM3ForceFieldTests
    {
        private const float bondunit = 71.94f;
        private const float bondcubic = -2.55f;
        private const float bondquartic = 3.793125f;
        private const float angleunit = 0.02191418f;
        private const float anglecubic = -0.014f;
        private const float anglequartic = 0.000056f;
        private const float anglepentic = -0.0000007f;
        private const float anglesextic = 0.000000022f;
        private const float RadToDegrees = 57.2957795f;
        private const float opbunit = 0.043844f;
        private const float torsionUnit = 0.5f;

        private const double tolerance = 1.0e-02;
        private string mm3File = "^/Assets/Data/mm3.xml";

        private Narupa.MD.Tests.TestHelper Helper;

        [SetUp]
        public void Initialize()
        {
            Helper = new Narupa.MD.Tests.TestHelper();
        }

        [Test()]
        /// <summary> Test that bond calculation is correct </summary>
        public void CalculateForceFieldBondTest()
        {
            MM3Data referenceData = new MM3Data(mm3File);

            InstantiatedTopology topology;
            List<int> types;

            List<Element> elements = new List<Element>();
            elements.Add((Element)PeriodicTable.Oxygen.Number);
            elements.Add((Element)PeriodicTable.Oxygen.Number);

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(0f, 0f, 0f));
            positions.Add(new Vector3(0.1f, 0f, 0f));

            BondList bonds = new BondList();
            bonds.AddBond(0, 1);

            topology = new InstantiatedTopology();
            topology.Bonds.AddRange(bonds);
            topology.Elements.AddRange(elements);
            topology.Positions.AddRange(positions);
            topology.GenerateMolecules();

            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            AtomicSystem system = new AtomicSystem(topology, properties, Helper.Reporter, false);
            types = new List<int>();
            types.Add(6);
            types.Add(6);

            MM3Topology mm3Top = new MM3Topology(topology.Molecules, types, referenceData, Helper.Reporter);
            MM3ForceField forcefield = new MM3ForceField(mm3Top);

            List<Vector3> forces = new List<Vector3>(topology.NumberOfParticles);
            List<Vector3> reference_forces = new List<Vector3>(topology.NumberOfParticles);
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                forces.Add(Vector3.Zero);
            }
            reference_forces.Add(new Vector3(-1078.57654f, 0f, 0f) * Units.KJPerKCal * Units.AngstromsPerNm);
            reference_forces.Add(new Vector3(1078.57654f, 0f, 0f) * Units.KJPerKCal * Units.AngstromsPerNm);

            float E = forcefield.CalculateForceField(system, forces);
            const float ref_E = 165.605408f * Units.KJPerKCal;
            Assert.AreEqual(ref_E, E, tolerance);

            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                Assert.AreEqual(reference_forces[i].X, forces[i].X, tolerance);
                Assert.AreEqual(reference_forces[i].Y, forces[i].Y, tolerance);
                Assert.AreEqual(reference_forces[i].Z, forces[i].Z, tolerance);
            }
        }

        [Test()]
        public void CalculateForceFieldAngleTest()
        {
            AngleStruct angles = new AngleStruct();

            angles.IndexI.Add(0);
            angles.IndexJ.Add(1);
            angles.IndexK.Add(2);
            float theta0 = 109.5f;
            float fc = 0.670f;
            angles.FC.Add(0.670f);
            angles.Theta0.Add(109.50f);

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(-0.1f, 0f, -1f));
            positions.Add(new Vector3(0f, 0.1f, 0f));
            positions.Add(new Vector3(0.1f, 0.12f, 0f));
            List<Element> elements = new List<Element>();
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);

            InstantiatedTopology flatTopology = new InstantiatedTopology();
            flatTopology.Positions.AddRange(positions);
            flatTopology.Elements.AddRange(elements);
            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            AtomicSystem system = new AtomicSystem(flatTopology, properties, Helper.Reporter, false);

            MM3Topology topology = new MM3Topology(3, null, null, angles);
            MM3ForceField forcefield = new MM3ForceField(topology);
            List<Vector3> forces = new List<Vector3>(positions.Count);
            for (int i = 0; i < positions.Count; i++)
            {
                forces.Add(Vector3.Zero);
            }
            List<Vector3> reference_forces = new List<Vector3>(positions.Count);

            Vector3 diffIJ = (positions[0] - positions[1]) * Units.AngstromsPerNm;
            Vector3 diffKJ = (positions[2] - positions[1]) * Units.AngstromsPerNm;

            float rij2 = diffIJ.LengthSquared;
            float rkj2 = diffKJ.LengthSquared;

            Vector3 cross = Vector3.Cross(diffKJ, diffIJ);
            float cross_length = cross.Length;
            float cosine = Vector3.Dot(diffIJ, diffKJ) / (float)Math.Sqrt(rij2 * rkj2);
            cosine = (float)Math.Min(1.0, Math.Max(-1.0f, cosine));
            float radToDegrees = 57.2958f;
            float angle = radToDegrees * (float)Math.Acos(cosine);

            float dt = angle - theta0;
            float dt2 = dt * dt;
            float dt3 = dt * dt2;
            float dt4 = dt2 * dt2;

            float ref_V = angleunit * fc * dt2 * (1.0f + anglecubic * dt + anglequartic * dt2 + anglepentic * dt3 + anglesextic * dt4);
            ref_V *= Units.KJPerKCal;

            float dVddt = angleunit * fc * dt * radToDegrees * (2.0f + 3.0f * anglecubic * dt + 4.0f * anglequartic * dt2 + 5.0f * anglepentic * dt3 + 6.0f * anglesextic * dt4);

            float terma = -1.0f * dVddt / (rij2 * cross_length);
            float termc = dVddt / (rkj2 * cross_length);
            Vector3 derivA = terma * Vector3.Cross(diffIJ, cross);
            Vector3 derivC = termc * Vector3.Cross(diffKJ, cross);
            Vector3 derivB = -1.0f * derivA - derivC;

            reference_forces.Add(-1.0f * derivA);
            reference_forces.Add(-1.0f * derivB);
            reference_forces.Add(-1.0f * derivC);

            float V = forcefield.CalculateForceField(system, forces);

            Assert.AreEqual(ref_V, V, tolerance);

            for (int i = 0; i < positions.Count; i++)
            {
                Assert.AreEqual(reference_forces[i].X * Units.KJPerKCal * Units.AngstromsPerNm, forces[i].X, tolerance);
                Assert.AreEqual(reference_forces[i].Y * Units.KJPerKCal * Units.AngstromsPerNm, forces[i].Y, tolerance);
                Assert.AreEqual(reference_forces[i].Z * Units.KJPerKCal * Units.AngstromsPerNm, forces[i].Z, tolerance);
            }
        }

        [Test()]
        public void CalculateForceFieldTorsionTest()
        {
            TorsionStruct torsions = new TorsionStruct();

            torsions.IndexI.Add(0);
            torsions.IndexJ.Add(1);
            torsions.IndexK.Add(2);
            torsions.IndexL.Add(3);

            torsions.V.Add(new Vector3(0.185f, 0.0f, 1f));

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(-0.1f, 0f, 0f));
            positions.Add(new Vector3(0f, 0f, -0.12f));
            positions.Add(new Vector3(0.1f, 0.12f, 0.12f));
            positions.Add(new Vector3(0.1f, -0.12f, 0f));
            List<Element> elements = new List<Element>();
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);

            InstantiatedTopology flatTopology = new InstantiatedTopology();
            flatTopology.Positions.AddRange(positions);
            flatTopology.Elements.AddRange(elements);
            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            AtomicSystem system = new AtomicSystem(flatTopology, properties, Helper.Reporter, false);
            MM3Topology topology = new MM3Topology(4, null, null, null, torsions);
            MM3ForceField forcefield = new MM3ForceField(topology);

            List<Vector3> forces = new List<Vector3>(positions.Count);
            for (int ii = 0; ii < positions.Count; ii++)
            {
                forces.Add(Vector3.Zero);
            }
            List<Vector3> reference_forces = new List<Vector3>(positions.Count);

            int i = 0;
            int j = 1;
            int k = 2;
            int l = 3;

            Vector3 diffJI = positions[j] - positions[i];
            Vector3 diffKJ = positions[k] - positions[j];
            Vector3 diffLK = positions[l] - positions[k];

            diffJI *= Units.AngstromsPerNm;
            diffKJ *= Units.AngstromsPerNm;
            diffLK *= Units.AngstromsPerNm;

            Vector3 crossT = Vector3.Cross(diffJI, diffKJ);
            Vector3 crossU = Vector3.Cross(diffKJ, diffLK);
            Vector3 crossTU = Vector3.Cross(crossT, crossU);
            float rt2 = crossT.LengthSquared;
            float ru2 = crossU.LengthSquared;

            float rtru = (float)Math.Sqrt(rt2 * ru2);

            float rcb = 0f;
            float cosine = 0f;
            float sine = 0f;
            if (rtru != 0f)
            {
                rcb = diffKJ.Length;
                cosine = Vector3.Dot(crossT, crossU) / rtru;
                sine = Vector3.Dot(diffKJ, crossTU) / (rcb * rtru); 
            }

            float v1 = torsions.V[0].X;
            float v2 = torsions.V[0].Y;
            float v3 = torsions.V[0].Z;
            float c1 = 1f;
            float s1 = 0f;
            float c2 = -1f;
            float s2 = 0f;
            float c3 = 1f;
            float s3 = 0f;

            float cosine2 = cosine * cosine - sine * sine;
            float sine2 = 2f * cosine * sine;
            float cosine3 = cosine * cosine2 - sine * sine2;
            float sine3 = cosine * sine2 + sine * cosine2;
            float phi1 = 1f + (cosine * c1 + sine * s1);
            float phi2 = 1f + (cosine2 * c2 + sine2 * s2);
            float phi3 = 1f + (cosine3 * c3 + sine3 * s3);
            float dphi1 = (cosine * s1 - sine * c1);
            float dphi2 = 2f * (cosine2 * s2 - sine2 * c2);
            float dphi3 = 3f * (cosine3 * s3 - sine3 * c3);

            float ref_V = torsionUnit * (v1 * phi1 + v2 * phi2 + v3 * phi3);
            ref_V *= Units.KJPerKCal;

            float dedphi = torsionUnit * (v1 * dphi1 + v2 * dphi2 + v3 * dphi3);

            Vector3 diffKI = positions[k] - positions[i];
            Vector3 diffLJ = positions[l] - positions[j];

            Vector3 dedt = dedphi * Vector3.Cross(crossT, diffKJ) / (rt2 * rcb);
            float dedxt = dedphi * (crossT.Y * diffKJ.Z - diffKJ.Y * crossT.Z) / (rt2 * rcb);
            float dedyt = dedphi * (crossT.Z * diffKJ.X - diffKJ.Z * crossT.X) / (rt2 * rcb);
            float dedzt = dedphi * (crossT.X * diffKJ.Y - diffKJ.X * crossT.Y) / (rt2 * rcb);

            Vector3 dedu = -dedphi * Vector3.Cross(crossU, diffKJ) / (ru2 * rcb);

            Vector3 dedi = (Vector3.Cross(dedt, diffKJ));
            Vector3 dedj = (Vector3.Cross(diffKI, dedt) + Vector3.Cross(dedu, diffLK));
            Vector3 dedk = (Vector3.Cross(dedt, diffJI) + Vector3.Cross(diffLJ, dedu));
            Vector3 dedl = (Vector3.Cross(dedu, diffKJ));
            reference_forces.Add(-1f * dedi);
            reference_forces.Add(-1f * dedj);
            reference_forces.Add(-1f * dedk);
            reference_forces.Add(-1f * dedl);

            float V = forcefield.CalculateForceField(system, forces);
            Assert.AreEqual(ref_V, V, tolerance);

            for (int ii = 0; ii < positions.Count; ii++)
            {
                Assert.AreEqual(reference_forces[ii].X * Units.KJPerKCal * Units.AngstromsPerNm, forces[ii].X, tolerance);
                Assert.AreEqual(reference_forces[ii].Y * Units.KJPerKCal * Units.AngstromsPerNm, forces[ii].Y, tolerance);
                Assert.AreEqual(reference_forces[ii].Z * Units.KJPerKCal * Units.AngstromsPerNm, forces[ii].Z, tolerance);
            }
        }

        [Test()]
        public void MM3ForceFieldTest()
        {
            try
            {
                MM3ForceField f = new MM3ForceField();
            }
            catch
            {
                Assert.Fail();
            }
        }
    }
}