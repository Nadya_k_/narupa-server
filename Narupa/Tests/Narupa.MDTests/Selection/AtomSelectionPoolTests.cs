﻿using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Narupa.MD.Selection;
using NUnit.Framework;
using Rug.Osc;

namespace Narupa.MD.Tests.Selection
{
    [TestFixture()]
    public class AtomSelectionPoolTests
    {
        private Narupa.MD.Tests.TestHelper helper;

        [SetUp]
        public void Initialize()
        {
            helper = new Narupa.MD.Tests.TestHelper();
        }

        [Test()]
        public void AtomSelectionPoolTest()
        {
            string simFileStr = "^/Assets/Simulations/TestSims/Atmosphere.xml";
            SimulationFile simFile = new SimulationFile(simFileStr, helper.Reporter);
            simFile.LoadSimulation();
            InstantiatedTopology topology = simFile.InstantiateTopology();

            AtomSelectionPool filteredAtoms = new AtomSelectionPool(new OscAddress("/selections"), topology);
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                Assert.AreEqual(i, filteredAtoms.VisibleAtoms[i]);
            }
        }

        [Test()]
        public void AtomSelectionPoolRemoveAllTest()
        {
            string simFileStr = "^/Assets/Simulations/TestSims/Atmosphere.xml";
            SimulationFile simFile = new SimulationFile(simFileStr, helper.Reporter);
            simFile.LoadSimulation();
            InstantiatedTopology topology = simFile.InstantiateTopology();

            AtomSelectionPool filteredAtoms = new AtomSelectionPool(new OscAddress("/selections"), topology);

            AtomSelection selection = new AtomSelection(filteredAtoms.Address, "all", topology.Addresses, false, new AtomSelectionString("//*", SelectionType.OscAddress));
            filteredAtoms.AddSelection(selection);
            selection.AtomSelectionChange += filteredAtoms.UpdateAtomSelections;

            selection.SetRenderSelection(false);

            Assert.AreEqual(0, filteredAtoms.VisibleAtoms.Count);

            filteredAtoms = new AtomSelectionPool(new OscAddress("/selections"), topology);
            selection = new AtomSelection(filteredAtoms.Address, "all", topology.Addresses, false, new AtomSelectionString(".*", SelectionType.Regex));
            filteredAtoms.AddSelection(selection);
            selection.AtomSelectionChange += filteredAtoms.UpdateAtomSelections;

            selection.SetRenderSelection(false);

            Assert.AreEqual(0, filteredAtoms.VisibleAtoms.Count);
        }

        [Test()]
        public void AtomSelectionPoolRenderTest()
        {
            string simFileStr = "^/Assets/Simulations/TestSims/Atmosphere.xml";
            SimulationFile simFile = new SimulationFile(simFileStr, helper.Reporter);
            simFile.LoadSimulation();
            InstantiatedTopology topology = simFile.InstantiateTopology();

            AtomSelectionPool filteredAtoms = new AtomSelectionPool(new OscAddress("/selections"), topology);

            AtomSelection selection = new AtomSelection(filteredAtoms.Address, "all", topology.Addresses, false, new AtomSelectionString("//*", SelectionType.OscAddress));
            filteredAtoms.AddSelection(selection);
            selection.AtomSelectionChange += filteredAtoms.UpdateAtomSelections;

            selection.SetRenderSelection(false);

            selection = new AtomSelection(filteredAtoms.Address, "ox", topology.Addresses, false, new AtomSelectionString(".*/O2/0/*", SelectionType.Regex));
            filteredAtoms.AddSelection(selection);
            selection.AtomSelectionChange += filteredAtoms.UpdateAtomSelections;
            selection.SetRenderSelection(true);

            Assert.AreEqual(2, filteredAtoms.VisibleAtoms.Count);
            for (int i = 0; i < 2; i++)
            {
                //first oxygen atom is 156 is full list.
                Assert.AreEqual(i + 156, filteredAtoms.VisibleAtoms[i]);
            }
        }
    }
}