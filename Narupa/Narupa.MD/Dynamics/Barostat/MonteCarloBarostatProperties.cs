﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Integrator;

namespace Narupa.MD.Dynamics.Barostat
{
    [XmlName("MonteCarloBarostat")]
    internal class MonteCarloBarostatProperties : BarostatPropertiesBase
    {
        public float RescaleFreq;

        public override string Name
        {
            get
            {
                return "MonteCarloBarostat";
            }
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);

            RescaleFreq = Helper.GetAttributeValue(node, "RescaleFrequency", 100);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            element = base.Save(context, element);
            Helper.AppendAttributeAndValue(element, "RescaleFrequency", RescaleFreq);
            return element;
        }

        public override IThermostatProperties Clone()
        {
            MonteCarloBarostatProperties m = new MonteCarloBarostatProperties();
            m.EquilibriumPressure = EquilibriumPressure;
            m.MaximumPressure = MaximumPressure;
            m.MinimumPressure = MinimumPressure;
            m.RescaleFreq = RescaleFreq;
            return m;
        }

        public override IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator)
        {
            return new MonteCarloBarostat(EquilibriumPressure, RescaleFreq, MinimumPressure, MaximumPressure);
        }

        public override bool Equals(IThermostatProperties other)
        {
            if (other is MonteCarloBarostatProperties == false) return false;
            MonteCarloBarostatProperties otherV = other as MonteCarloBarostatProperties;
            if (otherV.Name != Name) return false;
            if (Math.Abs(otherV.EquilibriumPressure - EquilibriumPressure) > 0.001f) return false;
            if (Math.Abs(otherV.MaximumPressure - MaximumPressure) > 0.001f) return false;
            if (Math.Abs(otherV.MinimumPressure - MinimumPressure) > 0.001f) return false;
            if (Math.Abs(otherV.RescaleFreq - RescaleFreq) > 0.001f) return false;
            return true;
        }
    }
}